'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const usersSchema = Schema({

    nombre: String,
    apellidos: String,
    username: String,
    password: String,
    email: String,
    telefono: String,
    tipoUsuario: {
        type: String,
        enum: ['Admin', 'Customer', 'Employee']
    }

}, {

    strict: false
})

module.exports = mongoose.model('users', usersSchema)