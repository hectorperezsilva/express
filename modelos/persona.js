var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Email = mongoose.SchemaTypes.Email;
var forms = require('forms-mongoose');

var AddressSchema = new Schema({
  category: {type: String, required: true, default: 'home', forms: {all:{}}},
  lines: {type: String, required: true, forms:{all:{widget:forms.widgets.textarea({rows:3})}}},
  city: {type: String, required: true}  
});
 
var PersonSchema = new Schema({
 
  confirmed: { type: Boolean, required: true, default: false },
  name: {
    first: { type: String, required: true, forms: {
      new: {},
      edit: {}
    }},
    last: { type: String, required: true, forms: {
      new: {},
      edit: {}
    }}
  },
  address: [AddressSchema]
});
 
var PersonModel = mongoose.model('Person', PersonSchema);
module.exports = PersonModel;