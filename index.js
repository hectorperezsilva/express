"use strict";
var mongoose = require('mongoose');
var express = require('express');
var bodyParser = require('body-parser');
var users = require('./modelos/users');
var persona = require('./modelos/persona');
var forms = require('forms-mongoose');

var form = forms.create(persona, 'new');

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET,POST,DELETE,PUT");
    next();
});
app.get('/', function (req, res) {
    res.json({ hola: 'mundos' });

    console.log (form);
});

//CREAR 

app.post('/api/usuarios', function(req, res){
    console.log(req.body);
    let user = new users(req.body);
    user.save((err, userStored) => {
        if(err){
            res.status(500).send({message: 'Error'})
        }
        res.status(200).send({user: userStored})
    })
});

//LEER TODOS LOS USUARIOS


app.get('/api/usuarios', function(req, res){
    
    users.find().lean().exec((err, users) => {
        return res.json(users);
    });
    
});

//LEER  USUARIO ESPECIFICO

app.get('/api/usuarios/:id', (req, res) => {

    var id = req.params.id;

    users.findById(id).lean().exec((err, usuarioEspecifico) => {

        if(err){
            console.log(err)
        }    

        return res.json(usuarioEspecifico);
    })


})


//ELIMINAR

app.delete('/api/usuarios/:id', (req, res) => {
    
    var id = req.params.id;

    users.findById(id, (err, usuarioEncontrado) =>{

        if(err){
            res.status(500).send({message: 'Error'})
        }

        usuarioEncontrado.remove(err => {

            if(err){

                res.status(500).send({message: 'Error'})

            }

            res.status(200).send({message: 'Usuario Eliminado'})
        })    

    })
    
});

//ACTUALIZAR

app.put('/api/usuarios/:id', (req, res) => {

    var id = req.params.id

    var updateBody = req.body

    users.findByIdAndUpdate(id, updateBody, (err, usuarioActualizado) =>{

        if(err){

            res.status(500).send({message: 'Error'})

        }

        res.status(200).send({message: 'Usuario Actualizado'})

    })

})



app.get('/hola', function (req, res) {
    res.json({ hola: 'mundo' });
});

app.post('/hola', function (req, res) {
    
    var usernameQualified = 'usuario';
    var passwordQualified = 'password';

    if(req.body.username === usernameQualified &&  req.body.password === passwordQualified){

        res.status(200).send();
        return;

    }
        res.status(403).send();
    
});


mongoose.connect("mongodb://localhost:27017/users", (err, res) => {

    if(err){
         throw err
    }
    console.log("conexión exitosa")     

});

app.listen(3000);